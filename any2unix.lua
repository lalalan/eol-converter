#!/usr/bin/env lua

--[[
 Converts mac (\r) or windows (\r\n) line endings to unix (\n)
 for stdin, specify /dev/stdin
 any2unix.lua files
]]

bufsize=1

function any2unix_simple(f)
 local str = f:read("*a")
  str = string.gsub(str,"\r\n","\n")
  str = string.gsub(str,"\r","\n")
  io.write( str )
end

function any2unix_buffered(f)
 local buffer, b
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer,"\r\n","\n")
  
  if b then -- last line had a \r$
   b=false
   -- because of b, \r was skipped
   -- so there is only one action left, to print or not to print
   if buffer:find("^[^\n]") then
     io.write("\r")
   end
  end
  
  if buffer:find("\r$") then
    b=true -- so we print \r in case of
    buffer = string.gsub(buffer,"\r","\n")
    io.write( buffer:sub(1,#buffer-1) )
  else
    buffer = string.gsub(buffer,"\r","\n")
    io.write( buffer )
  end
  
  
 end
end


function EOL2unix(File)
 local f = io.open(File)
 --any2unix_buffered(f)
 any2unix_simple(f)
 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   EOL2unix(File)
  end
else
  EOL2unix("/dev/stdin")
end
