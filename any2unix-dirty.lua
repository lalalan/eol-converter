#!/usr/bin/env lua

--[[
 This program converts any encoding to unix EOL(\n)
 you want stdin? you use /dev/stdin
 any2unix.lua files
]]

bufsize=1000

-- This is my first try, I've kept it for educational purposes
function EOL2unixbychar(f) -- finished
 local line = ""
 local b=false
 
 for c in f:lines(1) do
  if c ~= "\r" and c ~= "\n" then
    if b then b=false print(line) line="" end
    line = line .. c
    
   else -- its either \r or \n
    if c == "\n" then
     print(line)
     line = ""
     b=false
    else
     b=true
    end
  end
 end
  if string.len(line)>0 then io.write(line) line="" end
end

function EOL2unixbystream(f)
 local buffer,line
 local b=0
 for buffer in f:lines(bufsize) do
 -- buffer = string.gsub(string.gsub(buffer,"\r\n","\n",), "\n")
  buffer = string.gsub(buffer,"\r\n","\n")
  buffer = string.gsub(buffer,"\r","\n")
  -- print all lines\n
  for line in string.gmatch (buffer, "[^\n]*\n") do
   b = b + string.len(line)
   io.write(line)
  end
  -- if we have abc$ that is, a string with no newline left in the buffer
  -- just print that using io.write
  -- string.
  if b < string.len(buffer) then
   io.write(string.match(buffer,"[^\n]+$"))
  end
  b=0
 end
end


function EOL2unixbybuffer(f)
 local buffer
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer,"\r\n","\n")
  buffer = string.gsub(buffer,"\r","\n")
  io.write( buffer )
 end
end

-- Wrapper
function EOL2unix(File) -- finished
 local f = io.open(File)
 -- EOL2unixbychar(f) -- slow (and wrong?)
 -- EOL2unixbystream(f) -- ok
 EOL2unixbybuffer(f) -- fast
 io.close(f)
end

function test(File,fn)
 --EOL2unixbychar(File)
 EOL2unix(File)
 print("\n\n---------\n\n")
end

function uhh()
 if #arg>0 then 
   for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
    EOL2unix(File)
   end
 else
   EOL2unix("/dev/stdin")
 end
end

uhh()

--test("any2unix.text")
--test("any2unix.text2")
