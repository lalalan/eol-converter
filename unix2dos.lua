#!/usr/bin/env lua

--[[
 Converts unix (\n) to windows (\r\n) EOL
 for stdin, specify /dev/stdin
 unix2dos.lua files
]]

bufsize=1000

function unix2dos(File)
 local f = io.open(File)

 local buffer
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer, "\n", "\r\n")
  io.write( buffer ) -- EOL gets interpreted, so this will print correctly
 end

 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   unix2dos(File)
  end
else
  unix2dos("/dev/stdin")
end
