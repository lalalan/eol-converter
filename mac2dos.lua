#!/usr/bin/env lua

--[[
 Converts mac (\r) to dos (\r\n) EOL
 for stdin, specify /dev/stdin
 mac2dos.lua files
]]

bufsize=1000

function mac2dos(File)
 local f = io.open(File)

 local buffer
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer,"\r","\r\n")
  io.write( buffer ) -- EOL gets interpreted, so this will print correctly
 end

 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   mac2dos(File)
  end
else
  mac2dos("/dev/stdin")
end
