#!/usr/bin/env lua

--[[
 Converts unix (\n) to mac (\r) EOL
 for stdin, specify /dev/stdin
 unix2mac.lua files
]]

bufsize=1000

function unix2mac(File)
 local f = io.open(File)

 local buffer
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer, "\n", "\r")
  io.write( buffer ) -- EOL gets interpreted, so this will print correctly
 end

 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   unix2mac(File)
  end
else
  unix2mac("/dev/stdin")
end
