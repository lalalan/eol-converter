#!/usr/bin/env lua

--[[
 Converts windows (\r\n) to mac (\r) line endings
 for stdin, specify /dev/stdin
 dos2mac.lua files
]]

bufsize=1000

function dos2mac(File)
 local f = io.open(File)

 local buffer,b,start,skip
 for buffer in f:lines(bufsize) do
 -- EVERYTHING IS FUCKING BACKWARDS!
  start=1
  skip=0
  
  if b then -- last line had a \r$
   b=false
   if buffer:find("^\n") then
     io.write("\r") -- was \r\n, so converting now
     start=2 -- skip the \n
   end
  end
  
  if buffer:find("\r$") then
    b=true
    skip=1 -- skip last char
  end
  
  buffer = string.gsub(buffer,"\r\n","\r")
  io.write( buffer:sub(start,#buffer-skip ) )
 end

 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   dos2mac(File)
  end
else
  dos2mac("/dev/stdin")
end
