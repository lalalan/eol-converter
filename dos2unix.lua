#!/usr/bin/env lua

--[[
 Converts mac (\r) or windows (\r\n) line endings to unix (\n)
 for stdin, specify /dev/stdin
 dos2unix.lua files
]]

bufsize=1000

function dos2unix(File)
 local f = io.open(File)

 local buffer, b
 
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer,"\r\n","\n")
  
  if b then -- last line had a \r$
   b=false
   -- because of b, \r was skipped
   -- so there is only one action left, to print or not to print
   if buffer:find("^[^\n]") then
     io.write("\r")
   end
  end
  
  if buffer:find("\r$") then
    b=true -- so we print \r in case of
    io.write( buffer:sub(1,#buffer-1) )
  else
    io.write( buffer )
  end
  
 end  
 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
   dos2unix(File)
  end
else
  dos2unix("/dev/stdin")
end
