#!/usr/bin/env lua

--[[
 Converts mac (\r) or unix (\n) line endings to dos (\r\n)
 for stdin, specify /dev/stdin
 any2dos.lua files
]]

bufsize=1000

function any2dos(File)
 local f = io.open(File)

 local buffer
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer,"[\r\n]","\r\n")
  io.write( buffer )
 end

 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   any2dos(File)
  end
else
  any2dos("/dev/stdin")
end
