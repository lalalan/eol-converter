#!/usr/bin/env lua

--[[
 Converts mac (\r) to to unix (\n) EOL
 for stdin, specify /dev/stdin
 mac2unix.lua files
]]

bufsize=1000

function mac2unix(File)
 local f = io.open(File)

 local buffer
 for buffer in f:lines(bufsize) do
  buffer = string.gsub(buffer,"\r","\n")
  io.write( buffer ) -- EOL gets interpreted, so this will print correctly
 end

 io.close(f)
end

if #arg>0 then 
  for i,File in ipairs(arg) do
--    if File == "-" then File="/dev/stdin" end
   mac2unix(File)
  end
else
  mac2unix("/dev/stdin")
end
